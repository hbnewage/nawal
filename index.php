<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Home</title>
    <link rel="stylesheet" type="text/css" href="assets/css/main.min.css">
</head>
<body>

<div id="page-top"></div>
    
		<div id="top" class="navbar-fixed-top">
			<div id="top-bar">
				<div class="container">
					<div class="top-bar__info hidden-xs">
						<p>CALL US ON: <a href="tel:+973 123 456 7890">+973 123 456 7890</a></p>
					</div>
					<!-- // info  -->
					<div class="top-bar__links">
						<ul>
							<li><a href="#"><i class="fas fa-user"></i>MY ACCOUNT</a></li>
							<li><a href="#">FAQ</a></li>
							<li><a href="#">CONTACT US</a></li>
						</ul>
					</div>
					<!-- // links  -->
				</div>
				<!-- // container  -->
			</div>
			<!-- // top bar  -->
			<div id="menu-bar">
				<div class="container">
					<div class="branding">
						<a href="index.php"><img src="assets/img/logos/website-logo.png" alt=""></a>
					</div>
					<!-- // branding  -->
					<div id="main-menu">
						<?php include('inc/nav.php'); ?>
					</div>
					<!-- // main menu  -->
					<div id="actions-bar">
						<ul>
							<li><a href="#"><i class="far fa-search"></i></a></li>
						</ul>
					</div>
					<!-- // actions  -->
				</div>
				<!-- // container  -->
			</div>
			<!-- // menu bar  -->
		</div>
		<!-- // top bar  -->

        <header id="hero-banner">
            <div id="hero-slider">

                <div class="item hero-slide">
                    <img src="assets/img/bg/slide.jpg" alt="" class="slider-image">
                    <div class="caption">
                        <div class="container">
                            <div class="holder">
                                <h3 data-animation="fadeInUp" data-delay="0.3s">At Nawal Flowers we believe in celebrating each and every occasion.</h3>
                                <p data-animation="fadeInUp" data-delay="0.5s">Our endless passion and outstanding service help create romantic and luxurious events.</p>
                                <a href="#" class="btn-more" data-animation="fadeInUp" data-delay="0.7s"><span>READ MORE</span></a>
                            </div>
                            <!-- // holder  -->
                            <div class="image-holder">
                                <img src="assets/img/bg/phone.png" alt="" data-animation="fadeInDown" data-delay="0.2s">
                            </div>
                            <!-- // image  -->
                        </div>
                        <!-- // container  -->
                    </div>
                    <!-- // caption  -->
                </div>
                <!-- // item  -->

                <div class="item hero-slide">
                    <img src="assets/img/bg/slide.jpg" alt="" class="slider-image">
                    <div class="caption">
                        <div class="container">
                            <div class="holder">
                                <h3 data-animation="fadeInUp" data-delay="0.3s">At Nawal Flowers we believe in celebrating each and every occasion.</h3>
                                <p data-animation="fadeInUp" data-delay="0.5s">Our endless passion and outstanding service help create romantic and luxurious events.</p>
                                <a href="#" class="btn-more" data-animation="fadeInUp" data-delay="0.7s"><span>READ MORE</span></a>
                            </div>
                            <!-- // holder  -->
                            <div class="image-holder">
                                <img src="assets/img/bg/phone.png" alt="" data-animation="fadeInDown" data-delay="0.2s">
                            </div>
                            <!-- // image  -->
                        </div>
                        <!-- // container  -->
                    </div>
                    <!-- // caption  -->
                </div>
                <!-- // item  -->

                <div class="item hero-slide">
                    <img src="assets/img/bg/slide.jpg" alt="" class="slider-image">
                    <div class="caption">
                        <div class="container">
                            <div class="holder">
                                <h3 data-animation="fadeInUp" data-delay="0.3s">At Nawal Flowers we believe in celebrating each and every occasion.</h3>
                                <p data-animation="fadeInUp" data-delay="0.5s">Our endless passion and outstanding service help create romantic and luxurious events.</p>
                                <a href="#" class="btn-more" data-animation="fadeInUp" data-delay="0.7s"><span>READ MORE</span></a>
                            </div>
                            <!-- // holder  -->
                            <div class="image-holder">
                                <img src="assets/img/bg/phone.png" alt="" data-animation="fadeInDown" data-delay="0.2s">
                            </div>
                            <!-- // image  -->
                        </div>
                        <!-- // container  -->
                    </div>
                    <!-- // caption  -->
                </div>
                <!-- // item  -->                                

            </div>
            <!-- // hero slider  -->
        </header>
        <!-- // hero banner  -->

        <section id="about">
            <div class="container-fluid">
                <div class="content wow fadeInUp"  data-wow-duration="0.8s" data-wow-delay="0.5s">
                    <div class="holder">
                        <p>Nawal Flowers is a flower and event  planning company, committed to  producing and executing timeless  celebrations. Founded and managed by  Nawal Al Sabbagh with a deeply-rooted  passion for flowers and chocolate.</p>
                    </div>
                    <!-- // holder  -->
                </div>
                <!-- // content  -->
                <div class="video wow fadeInRight" data-wow-duration="1s" data-wow-delay="0.5s">
                    <div id="video-slider">

                        <div class="item">
                            <a href="#">
                                <img src="assets/img/misc/event.jpg" alt="" class="img-responsive">
                                <img src="assets/img/ico/yt.svg" class="icon" alt="">
                            </a>
                        </div>
                        <!-- // item  -->

                        <div class="item">
                            <a href="#">
                                <img src="assets/img/misc/event.jpg" alt="" class="img-responsive">
                                <img src="assets/img/ico/yt.svg" class="icon" alt="">
                            </a>
                        </div>
                        <!-- // item  -->
                        
                        <div class="item">
                            <a href="#">
                                <img src="assets/img/misc/event.jpg" alt="" class="img-responsive">
                                <img src="assets/img/ico/yt.svg" class="icon" alt="">
                            </a>
                        </div>
                        <!-- // item  -->                        

                    </div>
                    <!-- // video slider  -->
                </div>
                <!-- // video  -->
            </div>
            <!-- // fluid  -->
        </section>
        <!-- // about  -->

        <section id="featured">
            <div class="container">
                <div class="row">

                    <div class="col-md-4 col-sm-4">

                        <div class="featured-block wow fadeIn" data-wow-duration="0.8s" data-wow-delay="0.3s">
                            <a href="#">
                                <img src="assets/img/misc/chocolates.jpg" alt="" class="img-responsive">
                                <div class="caption">
                                    <h6>CHOCOLATE</h6>
                                </div>
                                <!-- // caption  -->
                            </a>
                            <!-- // href  -->
                        </div>
                        <!-- // featured block  -->

                        <div class="featured-block wow fadeIn" data-wow-duration="0.8s" data-wow-delay="0.5s">
                            <a href="#">
                                <img src="assets/img/misc/flower.jpg" alt="" class="img-responsive">
                                <div class="caption">
                                    <h6>Flower</h6>
                                </div>
                                <!-- // caption  -->
                            </a>
                            <!-- // href  -->
                        </div>
                        <!-- // featured block  -->                        

                    </div>
                    <!-- // col 1  -->
                    <div class="col-md-4 col-sm-4">

                        <div class="featured-block featured-long wow fadeIn" data-wow-duration="0.8s" data-wow-delay="0.7s">
                            <a href="#">
                                <img src="assets/img/misc/middle.jpg" alt="" class="img-responsive">
                                <div class="caption">
                                    <h6>HOME DECOR</h6>
                                </div>
                                <!-- // caption  -->
                            </a>
                            <!-- // href  -->
                        </div>
                        <!-- // featured block  -->   

                    </div>
                    <!-- // col 2  -->
                    <div class="col-md-4 col-sm-4">

                        <div class="featured-block wow fadeIn" data-wow-duration="0.8s" data-wow-delay="0.9s">
                            <a href="#">
                                <img src="assets/img/misc/right.jpg" alt="" class="img-responsive">
                                <div class="caption">
                                    <h6>EVENT MANAGEMENT</h6>
                                </div>
                                <!-- // caption  -->
                            </a>
                            <!-- // href  -->
                        </div>
                        <!-- // featured block  -->   
                        
                        <div class="featured-block wow fadeIn" data-wow-duration="0.8s" data-wow-delay="1s">
                            <a href="#">
                                <img src="assets/img/misc/right-2.jpg" alt="" class="img-responsive">
                                <div class="caption">
                                    <h6>GIFTS</h6>
                                </div>
                                <!-- // caption  -->
                            </a>
                            <!-- // href  -->
                        </div>
                        <!-- // featured block  -->                           

                    </div>
                    <!-- // col 3  -->

                </div>
                <!-- // row -->
            </div>
            <!-- // container  -->
        </section>
        <!-- // featured  -->

        <section id="best-sellers">
            <div class="container">
                <header class="wow fadeInUp">
                    <h4>Best Seller</h4>
                </header>
                <!-- // header  -->

                <div class="row">

                    <div class="col-md-3 col-sm-6">
                        <div class="product-block wow fadeInUp" data-wow-duration="0.5s" data-wow-delay="0.3s">
                            <div class="image-holder">
                                <a href="#">
                                    <img src="assets/img/misc/flower1.jpg" alt="">
                                    <button class="add-to-cart" onclick="cart.add('55');" tabindex="0">Add to Cart</button>
                                </a>
                            </div>
                            <!-- // image holder  -->
                            <div class="content">
                                <h4><a href="#">Tulip Wave</a></h4>
                                <span class="price">BHD 50.000 </span>
                            </div>
                            <!-- // content  -->
                        </div>
                    </div>
                    <!-- // col md 3  -->

                    <div class="col-md-3 col-sm-6">
                        <div class="product-block wow fadeInUp" data-wow-duration="0.5s" data-wow-delay="0.5s">
                            <div class="image-holder">
                                <a href="#">
                                    <img src="assets/img/misc/flower2.jpg" alt="">
                                    <button class="add-to-cart" onclick="cart.add('55');" tabindex="0">Add to Cart</button>
                                </a>
                            </div>
                            <!-- // image holder  -->
                            <div class="content">
                                <h4><a href="#">Touch of Gold</a></h4>
                                <span class="price">BHD 50.000 </span>
                            </div>
                            <!-- // content  -->
                        </div>
                    </div>
                    <!-- // col md 3  -->
                    
                    <div class="col-md-3 col-sm-6">
                        <div class="product-block wow fadeInUp" data-wow-duration="0.5s" data-wow-delay="0.7s">
                            <div class="image-holder">
                                <a href="#">
                                    <img src="assets/img/misc/flower3.jpg" alt="">
                                    <button class="add-to-cart" onclick="cart.add('55');" tabindex="0">Add to Cart</button>
                                </a>
                            </div>
                            <!-- // image holder  -->
                            <div class="content">
                                <h4><a href="#">Roses with Gypsophila</a></h4>
                                <span class="price">BHD 50.000 </span>
                            </div>
                            <!-- // content  -->
                        </div>
                    </div>
                    <!-- // col md 3  -->
                    
                    <div class="col-md-3 col-sm-6">
                        <div class="product-block wow fadeInUp" data-wow-duration="0.5s" data-wow-delay="0.9s">
                            <div class="image-holder">
                                <a href="#">
                                    <img src="assets/img/misc/flower4.jpg" alt="">
                                    <button class="add-to-cart" onclick="cart.add('55');" tabindex="0">Add to Cart</button>
                                </a>
                            </div>
                            <!-- // image holder  -->
                            <div class="content">
                                <h4><a href="#">Pink perfection</a></h4>
                                <span class="price">BHD 50.000 </span>
                            </div>
                            <!-- // content  -->
                        </div>
                    </div>
                    <!-- // col md 3  -->                    

                </div>
                <!-- // row  -->

            </div>
            <!-- // container  -->
        </section>
        <!-- //  best sellers  -->

        <section id="middle-cta">
            <div class="overlay"></div>
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1">
                        <div class="content">
                            <h3 class="wow fadeInUp">Event Management</h3>
                            <div class="holder wow fadeInUp">
                                <p>Whatever the milestone, we're here to help you create an unforgettable celebration.</p>
                                <a href="#" class="btn-more">READ MORE</a>
                            </div>
                        </div>
                        <!-- // content  -->
                    </div>
                </div>
                <!-- // row  -->
            </div>
            <!-- // container  -->
        </section>
        <!-- // middle cta  -->

        <section id="instagram-feed">
            <div class="container">
                <header class="wow fadeIn">
                    <h4><i class="fab fa-instagram"></i> NAWAL FLOWER</h4>
                </header>
                <!-- // hjeader  -->
                <div class="row">

                    <div class="col-md-3 col-sm-3">
                        <div class="image-holder wow fadeIn" data-wow-duration="0.5s" data-wow-delay="0.1s">
                            <a href="#">
                                <img src="assets/img/misc/insta.jpg" alt="" class="img-responsive">
                                <div class="overlay"><i class="fal fa-plus"></i></div>
                            </a>
                        </div>
                    </div>
                    <!-- // image holder  -->

                    <div class="col-md-3 col-sm-3">
                        <div class="image-holder wow fadeIn" data-wow-duration="0.5s" data-wow-delay="0.3s">
                            <a href="#">
                                <img src="assets/img/misc/insta1.jpg" alt="" class="img-responsive">
                                <div class="overlay"><i class="fal fa-plus"></i></div>
                            </a>
                        </div>
                    </div>
                    <!-- // image holder  -->
                    
                    <div class="col-md-3 col-sm-3">
                        <div class="image-holder wow fadeIn" data-wow-duration="0.5s" data-wow-delay="0.5s">
                            <a href="#">
                                <img src="assets/img/misc/insta.jpg" alt="" class="img-responsive">
                                <div class="overlay"><i class="fal fa-plus"></i></div>
                            </a>
                        </div>
                    </div>
                    <!-- // image holder  -->
                    
                    <div class="col-md-3 col-sm-3">
                        <div class="image-holder wow fadeIn" data-wow-duration="0.5s" data-wow-delay="0.7s">
                            <a href="#">
                                <img src="assets/img/misc/insta.jpg" alt="" class="img-responsive">
                                <div class="overlay"><i class="fal fa-plus"></i></div>
                            </a>
                        </div>
                    </div>
                    <!-- // image holder  -->                    

                </div>
                <!-- / /row  -->

                <footer class="wow fadeInUp">
                    <a href="#" class="btn-more"><span>FOLLOW US ON INSTAGRAM</span></a>
                </footer>

            </div>
        </section>
        <!-- // instagram feed  -->

	<?php include_once('inc/footer.php'); ?>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
	<script src="assets/js/custom-dist.js"></script> 

</body>
</html>
