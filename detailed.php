<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Home</title>
    <link rel="stylesheet" type="text/css" href="assets/css/main.min.css">
</head>
<body>

<div id="page-top"></div>
    
		<div id="top" class="navbar-fixed-top">
			<div id="top-bar">
				<div class="container">
					<div class="top-bar__info hidden-xs">
						<p>CALL US ON: <a href="tel:+973 123 456 7890">+973 123 456 7890</a></p>
					</div>
					<!-- // info  -->
					<div class="top-bar__links">
						<ul>
							<li><a href="#"><i class="fas fa-user"></i>MY ACCOUNT</a></li>
							<li><a href="#">FAQ</a></li>
							<li><a href="#">CONTACT US</a></li>
						</ul>
					</div>
					<!-- // links  -->
				</div>
				<!-- // container  -->
			</div>
			<!-- // top bar  -->
			<div id="menu-bar">
				<div class="container">
					<div class="branding">
						<a href="index.php"><img src="assets/img/logos/website-logo.png" alt=""></a>
					</div>
					<!-- // branding  -->
					<div id="main-menu">
						<?php include('inc/nav.php'); ?>
					</div>
					<!-- // main menu  -->
					<div id="actions-bar">
						<ul>
							<li><a href="#"><i class="far fa-search"></i></a></li>
						</ul>
					</div>
					<!-- // actions  -->
				</div>
				<!-- // container  -->
			</div>
			<!-- // menu bar  -->
		</div>
		<!-- // top bar  -->

        <div id="breadcrumb-nav">
            <div class="container">
                <ul>
                    <li><a href="#">Home</a></li>
                    <li><a href="#">Furniture</a></li>
                    <li><a href="#">Seating</a></li>
                    <li><a href="#">Sofas</a></li>
                </ul>
            </div>
        </div>
        <!-- // breadcrumb  -->

        <section id="single-product">
            <div class="container">
                <div class="row">

                    <div class="col-lg-5 col-md-6">
                        <div class="images-holder">
                            <div id="single-products-slider">

                                <div class="item">
                                    <img src="assets/img/misc/prodo.jpg" alt="" class="img-responsive">
                                </div>
                                <!-- // item  -->

                                <div class="item">
                                    <img src="assets/img/misc/prodo.jpg" alt="" class="img-responsive">
                                </div>
                                <!-- // item  -->

                                <div class="item">
                                    <img src="assets/img/misc/prodo.jpg" alt="" class="img-responsive">
                                </div>
                                <!-- // item  -->                                

                            </div>
                        </div>
                    </div>
                    <!-- // images  -->

                    <div class="col-lg-7 col-md-6">
                        <div class="product-content">
                            <h2>Roses with Gypsophila</h2>
                            <span class="price">BHD 30</span>
                            <div class="description">
                                <p>An arrangement containing mixed flowers.</p>
                                <ul>
                                    <li>Height: 58cm</li>
                                    <li>Width: 27cm</li>
                                    <li>Sell by: Quantity</li>
                                </ul>
                            </div>
                            <!-- // desc  -->
                            <div id="actin-btns">
                                <div class="counter-wrapper">
                                    <div class="count-input space-bottom">
                                    
                                    <input class="quantity" type="text" name="quantity" value="1"/>
                                    </div>
                                </div>
                                <!-- // counter  -->
                                <div class="cart-btn">
                                    <button type="submit" id="button-cart" data-loading-text="Loading...">Add to Cart</button>
                                </div>
                                <!-- // cart btn  -->
                                <div class="wishlist">
                                    <button class="wishlist" title="Add to Wish List">
                                    <i class="fal fa-heart"></i> <span>Add to Wish List</span>
                                    </button>
                                </div>
                            </div>
                            <!-- // btns  -->
                            <footer>
                                <small>Share: </small>
                            </footer>
                        </div>
                    </div>
                    <!-- // content  -->

                </div>
                <!-- / row  -->
            </div>
            <!-- // container  -->
        </section>
        <!-- // single product  -->

        <div id="related-products">
            <div class="container">
                <header>
                    <h4>Best Seller</h4>
                </header>
                <!-- // header  -->

                <div id="related-slider">

                    <div class="item">
                        <div class="product-block wow fadeInUp" data-wow-duration="0.5s" data-wow-delay="0.9s">
                            <div class="image-holder">
                                <a href="#">
                                    <img src="assets/img/misc/flower4.jpg" alt="">
                                    <button class="add-to-cart" onclick="cart.add('55');" tabindex="0">Add to Cart</button>
                                </a>
                            </div>
                            <!-- // image holder  -->
                            <div class="content">
                                <h4><a href="#">Pink perfection</a></h4>
                                <span class="price">BHD 50.000 </span>
                            </div>
                            <!-- // content  -->
                        </div>
                    </div>
                    <!-- // col md 3  -->      

                    <div class="item">
                        <div class="product-block wow fadeInUp" data-wow-duration="0.5s" data-wow-delay="0.9s">
                            <div class="image-holder">
                                <a href="#">
                                    <img src="assets/img/misc/flower1.jpg" alt="">
                                    <button class="add-to-cart" onclick="cart.add('55');" tabindex="0">Add to Cart</button>
                                </a>
                            </div>
                            <!-- // image holder  -->
                            <div class="content">
                                <h4><a href="#">Pink perfection</a></h4>
                                <span class="price">BHD 50.000 </span>
                            </div>
                            <!-- // content  -->
                        </div>
                    </div>
                    <!-- // col md 3  -->   
                    
                    <div class="item">
                        <div class="product-block wow fadeInUp" data-wow-duration="0.5s" data-wow-delay="0.9s">
                            <div class="image-holder">
                                <a href="#">
                                    <img src="assets/img/misc/flower3.jpg" alt="">
                                    <button class="add-to-cart" onclick="cart.add('55');" tabindex="0">Add to Cart</button>
                                </a>
                            </div>
                            <!-- // image holder  -->
                            <div class="content">
                                <h4><a href="#">Pink perfection</a></h4>
                                <span class="price">BHD 50.000 </span>
                            </div>
                            <!-- // content  -->
                        </div>
                    </div>
                    <!-- // col md 3  -->   
                    
                    <div class="item">
                        <div class="product-block wow fadeInUp" data-wow-duration="0.5s" data-wow-delay="0.9s">
                            <div class="image-holder">
                                <a href="#">
                                    <img src="assets/img/misc/flower3.jpg" alt="">
                                    <button class="add-to-cart" onclick="cart.add('55');" tabindex="0">Add to Cart</button>
                                </a>
                            </div>
                            <!-- // image holder  -->
                            <div class="content">
                                <h4><a href="#">Pink perfection</a></h4>
                                <span class="price">BHD 50.000 </span>
                            </div>
                            <!-- // content  -->
                        </div>
                    </div>
                    <!-- // col md 3  -->   
                    
                    <div class="item">
                        <div class="product-block wow fadeInUp" data-wow-duration="0.5s" data-wow-delay="0.9s">
                            <div class="image-holder">
                                <a href="#">
                                    <img src="assets/img/misc/flower4.jpg" alt="">
                                    <button class="add-to-cart" onclick="cart.add('55');" tabindex="0">Add to Cart</button>
                                </a>
                            </div>
                            <!-- // image holder  -->
                            <div class="content">
                                <h4><a href="#">Pink perfection</a></h4>
                                <span class="price">BHD 50.000 </span>
                            </div>
                            <!-- // content  -->
                        </div>
                    </div>
                    <!-- // col md 3  -->                       

                </div>
                <!-- // related slider  -->

            </div>
        </div>
        <!-- // related products  -->

	<?php include_once('inc/footer.php'); ?>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js"></script> 
    <script src="assets/js/custom-dist.js"></script> 
    

</body>
</html>
