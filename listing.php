<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Home</title>
    <link rel="stylesheet" type="text/css" href="assets/css/main.min.css">
</head>
<body>

<div id="page-top"></div>
    
		<div id="top" class="navbar-fixed-top">
			<div id="top-bar">
				<div class="container">
					<div class="top-bar__info hidden-xs">
						<p>CALL US ON: <a href="tel:+973 123 456 7890">+973 123 456 7890</a></p>
					</div>
					<!-- // info  -->
					<div class="top-bar__links">
						<ul>
							<li><a href="#"><i class="fas fa-user"></i>MY ACCOUNT</a></li>
							<li><a href="#">FAQ</a></li>
							<li><a href="#">CONTACT US</a></li>
						</ul>
					</div>
					<!-- // links  -->
				</div>
				<!-- // container  -->
			</div>
			<!-- // top bar  -->
			<div id="menu-bar">
				<div class="container">
					<div class="branding">
						<a href="index.php"><img src="assets/img/logos/website-logo.png" alt=""></a>
					</div>
					<!-- // branding  -->
					<div id="main-menu">
						<?php include('inc/nav.php'); ?>
					</div>
					<!-- // main menu  -->
					<div id="actions-bar">
						<ul>
							<li><a href="#"><i class="far fa-search"></i></a></li>
						</ul>
					</div>
					<!-- // actions  -->
				</div>
				<!-- // container  -->
			</div>
			<!-- // menu bar  -->
		</div>
		<!-- // top bar  -->

        <header id="inner-header">
            <div class="container">
                <div class="content">
                    <h1 class="wow fadeIn">Flowers</h1>
                    <div class="breadcrumb wow fadeInUp">
                        <ul>
                            <li><a href="#">Home</a></li>
                            <li><a href="#">Flowers</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </header>
        <!-- // inner header  -->

        <section id="products-listing__page">
            <div class="container">
                <div class="row">

                    <div class="col-lg-9 col-lg-push-3 col-md-8 col-md-push-4 col-sm-8 col-sm-push-4">
                        <div id="main-listing">

                            <div id="filter-bar">
                                <div class="row">

                                    <div class="col-sm-6 col-md-6 col-sm-6">
                                        <div class="results-block">
                                            <small>Showing 1 to 9 of 63 (7 Pages)</small>
                                        </div>
                                    </div>
                                    <!-- // results  -->

                                    <div class="col-sm-6 col-md-6 col-sm-6">
                                        <div class="sort">
                                            <small>Sort</small>
                                            <select name="" id="">
                                                <option value="">Name (A - Z)</option>
                                                <option value="">Name (Z - A)</option>
                                                <option value="">Price (Low &gt; High)</option>
                                                <option value="">Price (High &gt; Low)</option>
                                                <option value="">Model (A - Z)</option>
                                                <option value="">Model (Z - A)</option>
                                            </select>
                                        </div>
                                    </div>
                                    <!-- // sort  -->

                                </div>
                            </div>
                            <!-- // filter bar  -->

                <div class="row">

                    <div class="col-md-4 col-sm-6">
                        <div class="product-block wow fadeInUp" data-wow-duration="0.5s" data-wow-delay="0.1s">
                            <div class="image-holder">
                                <a href="#">
                                    <img src="assets/img/misc/flower1.jpg" alt="">
                                    <button class="add-to-cart" onclick="cart.add('55');" tabindex="0">Add to Cart</button>
                                </a>
                            </div>
                            <!-- // image holder  -->
                            <div class="content">
                                <h4><a href="#">Tulip Wave</a></h4>
                                <span class="price">BHD 50.000 </span>
                            </div>
                            <!-- // content  -->
                        </div>
                    </div>
                    <!-- // col md 3  -->

                    <div class="col-md-4 col-sm-6">
                        <div class="product-block wow fadeInUp" data-wow-duration="0.5s" data-wow-delay="0.2s">
                            <div class="image-holder">
                                <a href="#">
                                    <img src="assets/img/misc/flower2.jpg" alt="">
                                    <button class="add-to-cart" onclick="cart.add('55');" tabindex="0">Add to Cart</button>
                                </a>
                            </div>
                            <!-- // image holder  -->
                            <div class="content">
                                <h4><a href="#">Touch of Gold</a></h4>
                                <span class="price">BHD 50.000 </span>
                            </div>
                            <!-- // content  -->
                        </div>
                    </div>
                    <!-- // col md 3  -->
                    
                    <div class="col-md-4 col-sm-6">
                        <div class="product-block wow fadeInUp" data-wow-duration="0.5s" data-wow-delay="0.3s">
                            <div class="image-holder">
                                <a href="#">
                                    <img src="assets/img/misc/flower3.jpg" alt="">
                                    <button class="add-to-cart" onclick="cart.add('55');" tabindex="0">Add to Cart</button>
                                </a>
                            </div>
                            <!-- // image holder  -->
                            <div class="content">
                                <h4><a href="#">Roses with Gypsophila</a></h4>
                                <span class="price">BHD 50.000 </span>
                            </div>
                            <!-- // content  -->
                        </div>
                    </div>
                    <!-- // col md 3  -->
                    
                    <div class="col-md-4 col-sm-6">
                        <div class="product-block wow fadeInUp" data-wow-duration="0.5s" data-wow-delay="0.4s">
                            <div class="image-holder">
                                <a href="#">
                                    <img src="assets/img/misc/flower4.jpg" alt="">
                                    <button class="add-to-cart" onclick="cart.add('55');" tabindex="0">Add to Cart</button>
                                </a>
                            </div>
                            <!-- // image holder  -->
                            <div class="content">
                                <h4><a href="#">Pink perfection</a></h4>
                                <span class="price">BHD 50.000 </span>
                            </div>
                            <!-- // content  -->
                        </div>
                    </div>
                    <!-- // col md 3  -->                 
                    
                    
                    <div class="col-md-4 col-sm-6">
                        <div class="product-block wow fadeInUp" data-wow-duration="0.5s" data-wow-delay="0.5s">
                            <div class="image-holder">
                                <a href="#">
                                    <img src="assets/img/misc/flower4.jpg" alt="">
                                    <button class="add-to-cart" onclick="cart.add('55');" tabindex="0">Add to Cart</button>
                                </a>
                            </div>
                            <!-- // image holder  -->
                            <div class="content">
                                <h4><a href="#">Pink perfection</a></h4>
                                <span class="price">BHD 50.000 </span>
                            </div>
                            <!-- // content  -->
                        </div>
                    </div>
                    <!-- // col md 3  -->        
                    
                    
                    <div class="col-md-4 col-sm-6">
                        <div class="product-block wow fadeInUp" data-wow-duration="0.5s" data-wow-delay="0.6s">
                            <div class="image-holder">
                                <a href="#">
                                    <img src="assets/img/misc/flower4.jpg" alt="">
                                    <button class="add-to-cart" onclick="cart.add('55');" tabindex="0">Add to Cart</button>
                                </a>
                            </div>
                            <!-- // image holder  -->
                            <div class="content">
                                <h4><a href="#">Pink perfection</a></h4>
                                <span class="price">BHD 50.000 </span>
                            </div>
                            <!-- // content  -->
                        </div>
                    </div>
                    <!-- // col md 3  -->        
                    
                    
                    <div class="col-md-4 col-sm-6">
                        <div class="product-block wow fadeInUp" data-wow-duration="0.5s" data-wow-delay="0.7s">
                            <div class="image-holder">
                                <a href="#">
                                    <img src="assets/img/misc/flower4.jpg" alt="">
                                    <button class="add-to-cart" onclick="cart.add('55');" tabindex="0">Add to Cart</button>
                                </a>
                            </div>
                            <!-- // image holder  -->
                            <div class="content">
                                <h4><a href="#">Pink perfection</a></h4>
                                <span class="price">BHD 50.000 </span>
                            </div>
                            <!-- // content  -->
                        </div>
                    </div>
                    <!-- // col md 3  -->        
                    
                    
                    <div class="col-md-4 col-sm-6">
                        <div class="product-block wow fadeInUp" data-wow-duration="0.5s" data-wow-delay="0.75s">
                            <div class="image-holder">
                                <a href="#">
                                    <img src="assets/img/misc/flower4.jpg" alt="">
                                    <button class="add-to-cart" onclick="cart.add('55');" tabindex="0">Add to Cart</button>
                                </a>
                            </div>
                            <!-- // image holder  -->
                            <div class="content">
                                <h4><a href="#">Pink perfection</a></h4>
                                <span class="price">BHD 50.000 </span>
                            </div>
                            <!-- // content  -->
                        </div>
                    </div>
                    <!-- // col md 3  -->       
                    
                    <div class="col-md-4 col-sm-6">
                        <div class="product-block wow fadeInUp" data-wow-duration="0.5s" data-wow-delay="0.80s">
                            <div class="image-holder">
                                <a href="#">
                                    <img src="assets/img/misc/flower4.jpg" alt="">
                                    <button class="add-to-cart" onclick="cart.add('55');" tabindex="0">Add to Cart</button>
                                </a>
                            </div>
                            <!-- // image holder  -->
                            <div class="content">
                                <h4><a href="#">Pink perfection</a></h4>
                                <span class="price">BHD 50.000 </span>
                            </div>
                            <!-- // content  -->
                        </div>
                    </div>
                    <!-- // col md 3  -->                           

                </div>
                <!-- // row  -->                            

                        </div>
                    </div>
                    <!-- // main listing  -->

                    <div class="col-lg-3 col-lg-pull-9 col-md-4 col-md-pull-8 col-sm-4 col-sm-pull-8">
                        <aside id="product-sidebar" class="wow fadeIn">
                            <h5>FILTER BY</h5>
                            <div id="filter-accordion">

                                <h4>FLOWER TYPE</h4>
                                <div class="filter-content">
                                    <label class="control control--checkbox">Arabic (2)
                                    <input type="checkbox"/>
                                    <div class="control__indicator"></div>
                                    </label>
                                    <label class="control control--checkbox">Oriental (1)
                                    <input type="checkbox"/>
                                    <div class="control__indicator"></div>
                                    </label>
                                    <label class="control control--checkbox">Western (4)
                                    <input type="checkbox"/>
                                    <div class="control__indicator"></div>
                                    </label>
                                </div>
                                <!-- // filter conten  -->

                                <h4>OCCASION</h4>
                                <div class="filter-content">
                                    <label class="control control--checkbox">Arabic (2)
                                    <input type="checkbox"/>
                                    <div class="control__indicator"></div>
                                    </label>
                                    <label class="control control--checkbox">Oriental (1)
                                    <input type="checkbox"/>
                                    <div class="control__indicator"></div>
                                    </label>
                                    <label class="control control--checkbox">Western (4)
                                    <input type="checkbox"/>
                                    <div class="control__indicator"></div>
                                    </label>
                                </div>
                                <!-- // filter conten  -->          
                                
                                <h4>PRICE</h4>
                                <div class="filter-content">
                                    <label class="control control--checkbox">Arabic (2)
                                    <input type="checkbox"/>
                                    <div class="control__indicator"></div>
                                    </label>
                                    <label class="control control--checkbox">Oriental (1)
                                    <input type="checkbox"/>
                                    <div class="control__indicator"></div>
                                    </label>
                                    <label class="control control--checkbox">Western (4)
                                    <input type="checkbox"/>
                                    <div class="control__indicator"></div>
                                    </label>
                                </div>
                                <!-- // filter conten  -->                                  

                            </div>
                            <!-- // filter accordion  -->
                            <footer>
                                <input type="reset" class="bf-buttonclear" onclick="BrainyFilter.reset();return false;" value="Reset All" placeholder="">
                            </footer>
                        </aside>
                    </div>
                    <!-- // sidebar  -->

                </div>
                <!-- // row  -->
            </div>
            <!-- // container  -->
        </section>
        <!-- // listing page  -->

	<?php include_once('inc/footer.php'); ?>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js"></script> 
    <script src="assets/js/custom-dist.js"></script> 
    

</body>
</html>
